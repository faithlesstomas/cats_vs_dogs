# MLOps Task


## Training

```sh
python ./training/train.py <path_to_data_dir>
```
`path_to_data_dir` must contain `train` and `test` directories
which should contain training and testing datasets
(jpg files of cats and dogs) respectively.


## Building container image

```sh
podman build .
```

## Example usage:

TODO:

```sh
curl -v -H "Content-Type: application/json" \
  -X POST -d '{"image_url":"https://upload.wikimedia.org/wikipedia/commons/a/a5/Red_Kitten_01.jpg"}' \
  http://0.0.0.0/predict
```
