FROM ubuntu:22.04

RUN apt-get update
RUN apt-get install -y python3-pip uvicorn
RUN rm -rf /var/lib/apt/lists/*


COPY ./requirements.txt .

RUN pip3 install --no-cache-dir --upgrade -r requirements.txt
RUN pip3 install torch torchvision --index-url https://download.pytorch.org/whl/cpu

RUN mkdir -p /app/training/

COPY main.py /app/
COPY ./training/model.pth /app/training/
COPY ./training/*.py /app/training/

WORKDIR /app

EXPOSE 80/tcp

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
