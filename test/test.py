import pytest
import requests
import yaml

try:
    with open('test/url_samples.yaml', 'r') as f:
        data = yaml.safe_load(f)
    assert 'cats' in data
    assert 'dogs' in data
except:
    raise ValueError

@pytest.mark.parametrize("url", data['cats'])
def test_model_for_cats(url, api_url):
    payload = {'image_url': url}
    r = requests.post(f'{api_url}', json=payload)

    assert r.status_code == 200

    out = r.json()
    assert out['image_url'] == url
    assert 'prediction' in out
    pred = out['prediction']

    assert pred['cat'] > pred['dog']

@pytest.mark.parametrize("url", data['dogs'])
def test_model_for_dogs(url, api_url):
    payload = {'image_url': url}
    r = requests.post(f'{api_url}', json=payload)

    assert r.status_code == 200

    out = r.json()
    assert out['image_url'] == url
    assert 'prediction' in out
    pred = out['prediction']

    assert pred['dog'] > pred['cat']
