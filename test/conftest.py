import pytest
import argparse

def pytest_addoption(parser):
    parser.addoption("--name", action="store")
    parser.addoption(
        '--app-host', type=str, default='0.0.0.0',
        help='app service host to test',
        dest='app_host', required=False
    )
    parser.addoption(
        '--app-port', type=str, default='80',
        help='app service port to test',
        dest='app_port', required=False
    )

@pytest.fixture(scope='session')
def api_url(request):
    host = request.config.option.app_host
    port = request.config.option.app_port
    return 'http://' + host + ':' + port + '/predict'
