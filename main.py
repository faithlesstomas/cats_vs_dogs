import json
import urllib.request
# import requests
import uvicorn
import torch
import logging

from fastapi import FastAPI
from fastapi.responses import JSONResponse

from pydantic import BaseModel
from torchvision import transforms

import training
from training import models
from training.data import LoadData
from torch.utils.data import DataLoader

DEVICE = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

logging.basicConfig(level=logging.DEBUG)

# log = logging.getLogger("uvicorn")
log = logging.getLogger("uvicorn.access")
# log = logging.getLogger("fastapi")
# logger = logging.getLogger("uvicorn")


# image url for test:
# https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/American_Shorthair.jpg/1200px-American_Shorthair.jpg

# curl cmd:
# curl -v -H "Content-Type: application/json" -X POST \
# -d '{"image_url":"https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/American_Shorthair.jpg/1200px-American_Shorthair.jpg"}' http://0.0.0.0/predict

def img_to_tensor(im):
    resized_img = im.resize((224,224))
    transformer = transforms.Compose(
        [
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225])
        ]
    )

    tensor_img = transformer(resized_img)
    return tensor_img

def make_prediction(file_path):

    model = models.Resnet18Model()
    model.load_state_dict(torch.load('training/model.pth'))
    model.eval()

    correct = 0

    loader = DataLoader(LoadData(2, file_path))

    for idx, (data, target) in enumerate(loader):
        data = data.to(DEVICE)

        target = target.to(DEVICE)
        output = model(data)

        pred = output.argmax(dim=1, keepdim=True)
        target = target.argmax(dim=1, keepdim=True)

        result = {
            'dog': output[0,0].item(),
            'cat': output[0,1].item()
        }

        correct += pred.eq(target.view_as(pred)).sum().item()

        log.debug(result)

    return result


class ImageData(BaseModel):
    image_url: str

# class ImagePredict(ImageData):
#     prediction: dict

class Prediction(BaseModel):
    cat: float
    dog: float

class Resp(BaseModel):
    image_url: str
    prediction: Prediction


app = FastAPI()



@app.get("/")
async def read_root():
    return {"message": "Hello in Cats and Dogs image predictor!"}

@app.post("/predict", response_model=Resp)
async def process_image(data: ImageData):
    log.debug(data.image_url)

    local_file_path, headers = urllib.request.urlretrieve(data.image_url)
    log.debug(local_file_path)

    pred = make_prediction(local_file_path)

    result = {
        'image_url': data.image_url,
        'prediction': pred
    }

    return JSONResponse(content=result)


if __name__ == '_main_':
    log.info('start service - test')
    uvcorn.run('main:app', port='8000', log_level='debug')
